import React from 'react';
import { Router, Route, IndexRedirect, browserHistory } from 'react-router';
import App from './containers/App';

function setModule(done) {
  return function setter(module) {
    return done(null, module.default);
  };
}

const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRedirect to="/1" />
      <Route
        path="planet/:planetId"
        getComponent={(nextState, done) =>
        System.import('./containers/People').then(setModule(done))}
      />
      <Route
        path=":page"
        getComponent={(nextState, done) =>
        System.import('./containers/Planets').then(setModule(done))}
      />
    </Route>
  </Router>
);

export default routes;
