import planets from './planets';
import planetsList from './planetsList';
import people from './people';

export default {
  planets,
  planetsList,
  people,
};
