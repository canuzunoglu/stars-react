import { createSelector } from 'reselect';
import config from '../config';
import { planetSelector } from './planets';

export const FETCH_PERSON = 'PEOPLE/FETCH_PERSON';
export const SET_PERSON = 'PEOPLE/SET_PERSON';

export const createPeopleHash = (id, API = config.API) => `${API}/people/${id}/`;

export default function reducer(state = {}, { payload, type }) {
  switch (type) {
    case FETCH_PERSON: {
      const { hash } = payload;
      return {
        ...state,
        [hash]: {
          ready: false,
          error: false,
        },
      };
    }
    case SET_PERSON: {
      const { url } = payload;
      const error = payload.error || false;
      return {
        ...state,
        [url]: {
          ...payload,
          ready: !error,
          error,
        },
      };
    }
    default:
      return state;
  }
}

export const fetchPerson = (url) => (dispatch, getState) => {
  const { people } = getState();
  if (people[url] || !url) {
    return;
  }
  dispatch({ type: FETCH_PERSON, payload: Promise.resolve({ hash: url }) });
  dispatch({
    type: SET_PERSON,
    payload: fetch(url).then(response => response.json())
  });
};

export const peopleStoreSelector = (state) => state.people;

export const peopleSelector = createSelector(
  [peopleStoreSelector, planetSelector],
  (people, planet) => (planet && planet.ready) && planet.residents.map(url => people[url]));
