import { createSelector } from 'reselect';
import { compose } from 'redux';
import config from '../config';
import getUrlQueries from '../utils/getUrlQueries';

export const FETCH_PLANETS_LIST = 'PLANETS_LIST/FETCH_PLANETS_LIST';
export const SET_PLANETS_LIST = 'PLANETS_LIST/SET_PLANETS_LIST';

export const createPlanetsPageHash = (page, API = config.API) => `${API}/planets/?page=${page}`;
export const parsePlanetsPageHash = compose(({ page }) => page, getUrlQueries);

export default function reducer(state = {}, { payload, type }) {
  switch (type) {
    case FETCH_PLANETS_LIST: {
      const { hash } = payload;
      return {
        ...state,
        [hash]: {
          next: null,
          prev: null,
          ready: false,
        },
      };
    }
    case SET_PLANETS_LIST: {
      const { hash, next, previous, results } = payload;
      return {
        ...state,
        [hash]: {
          next,
          previous,
          nextPage: next && parsePlanetsPageHash(next),
          prevPage: previous && parsePlanetsPageHash(previous),
          planets: results.map(planet => planet.url),
          ready: payload.error || true,
        },
      };
    }
    default:
      return state;
  }
}

export const fetchPlanetsList = (page) => (dispatch, getState) => {
  const { planetsList } = getState();
  const url = createPlanetsPageHash(page);
  if (planetsList[url]) {
    return;
  }
  dispatch({ type: FETCH_PLANETS_LIST, payload: Promise.resolve({ hash: url }) });
  dispatch({
    type: SET_PLANETS_LIST,
    payload: fetch(url)
             .then(response => response.json())
             .then(json => ({ ...json, hash: url }))
  });
};

export const planetsListStoreSelector = (state) => state.planetsList;

export const planetsListHashSelector = (state, { params: { page } }) => createPlanetsPageHash(page);

export const planetsPageSelector = createSelector(
  [planetsListStoreSelector, (state) => state.planets, planetsListHashSelector],
  (planetsList, planets, planetsListHash) => {
    const list = planetsList[planetsListHash];
    const mappedPlanets = (list && list.planets) && list.planets.map(hash => planets[hash]);
    return {
      ...list,
      planets: mappedPlanets,
    };
  }
);
