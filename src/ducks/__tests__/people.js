import reducer, { SET_PERSON, FETCH_PERSON } from '../people';

const UNKNOWN_ACTION = 'UNKNOWN_ACTION';


describe('People duck', () => {
  describe('Reducer', () => {
    it('should by default return an object with an all empty object', () => {
      expect(reducer({}, UNKNOWN_ACTION))
        .toBeInstanceOf(Object);
    });

    describe('FETCH_PERSON', () => {
      it('should return ready:false after an FETCH_PERSON action', () => {
        const newState = reducer({}, { type: FETCH_PERSON, payload: { hash: 'test' } });
        expect(newState).toBeInstanceOf(Object);
        expect(newState.test).toBeInstanceOf(Object);
        expect(newState.test).toEqual({ ready: false, error: false });
      });
    });

    describe('SET_PERSON', () => {
      it('should add people from actions payload after an SET_PERSON action', () => {
        const state = {};
        const person = { id: 'someId', name: 'Lorem', url: 'test' };
        const action = { type: SET_PERSON, payload: person };
        const newState = reducer(state, action);

        expect(newState).not.toEqual(state);
        expect(newState).toBeInstanceOf(Object);
        expect(newState.test.name).toEqual('Lorem');
      });

      it('should return error if receives error', () => {
        const state = { };
        const newState = reducer(state, { type: SET_PERSON, payload: { url: 'test', error: true } });
        expect(newState.test.error).toEqual(true);
      });
    });
  });
});
