import { createSelector } from 'reselect';
import { keyBy } from 'lodash';
import Url from 'url';
import config from '../config';
import { SET_PLANETS_LIST } from './planetsList';

export const FETCH_PLANET = 'PLANETS/FETCH_PLANET';
export const SET_PLANET = 'PLANETS/SET_PLANET';

export const createPlanetHash = (id, API = config.API) => `${API}/planets/${id}/`;
export const parsePlanetId = (url) => {
  const urlArr = Url.parse(url).pathname.split('/');
  return urlArr[urlArr.length - 2];
};

export const planetsSelector = (state) => state.planets;

export default function reducer(state = {}, { payload, type }) {
  switch (type) {
    case FETCH_PLANET: {
      const { hash } = payload;
      return {
        ...state,
        [hash]: {
          ready: false,
          error: false,
        },
      };
    }
    case SET_PLANET: {
      const { url } = payload;
      const error = payload.error || false;
      return {
        ...state,
        [url]: {
          ...payload,
          ready: !error,
          error,
        },
      };
    }
    case SET_PLANETS_LIST: {
      const { results } = payload;
      const error = payload.error || false;
      return {
        ...state,
        ...keyBy(results.map(planet => ({ ...planet, error, ready: !error })), 'url'),
      };
    }
    default:
      return state;
  }
}

export const fetchPlanet = (planetId) => (dispatch, getState) => {
  const { planets } = getState();
  const url = createPlanetHash(planetId);
  if (planets[url] || !planetId) {
    return;
  }
  dispatch({ type: FETCH_PLANET, payload: Promise.resolve({ hash: url }) });
  dispatch({
    type: SET_PLANET,
    payload: fetch(url).then(response => response.json())
  });
};

export const planetsStoreSelector = (state) => state.planets;
export const planetIdSelector = (state, { params: { planetId } }) => planetId;

export const planetSelector = createSelector(
  [planetsStoreSelector, planetIdSelector],
  (planets, planetId) => planets[createPlanetHash(planetId)]);
