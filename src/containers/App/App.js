import React, { Component, PropTypes } from 'react';
import defaultStyles from './App.css';
import logo from '../../sw.png';

export class AppContainer extends Component {
  static propTypes = {
    children: PropTypes.any,
    styles: PropTypes.object,
  }

  static defaultProps = {
    styles: defaultStyles,
  }

  render() {
    const { styles, children } = this.props;
    return (
      <div>
        <div className={styles.main}>
          <img src={logo} alt="Star Wars" className={styles.logo} />
          {children}
        </div>
      </div>
    );
  }
}

export default AppContainer;
