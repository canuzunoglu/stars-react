import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchPlanet, planetSelector } from '../../ducks/planets';
import { fetchPerson, peopleSelector } from '../../ducks/people';
import Person from '../../components/Person';
import Loading from '../../components/Loading';

export class PeopleContainer extends Component {
  static propTypes = {
    fetchPlanet: PropTypes.func.isRequired,
    fetchResidents: PropTypes.func.isRequired,
    planet: PropTypes.objectOf(PropTypes.string).isRequired,
    people: PropTypes.arrayOf(PropTypes.object),
    params: PropTypes.objectOf(PropTypes.string).isRequired,
    ready: PropTypes.bool,
  };

  static defaultProps = {
    ready: false,
    people: [],
  }

  componentDidMount() {
    const { ready, planet, params: { planetId } } = this.props;
    this.props.fetchPlanet(planetId);
    if (ready) {
      this.props.fetchResidents(planet.residents);
    }
  }

  componentWillReceiveProps({ ready, planet, params: { planetId } }) {
    if (ready && this.props.planet.name !== planet.name) {
      this.props.fetchResidents(planet.residents);
    }

    if (planetId !== this.props.params.planetId) {
      this.props.fetchPlanet(planetId);
    }
  }

  render() {
    const { ready, planet, people } = this.props;
    if (planet && ready) {
      return (
        <div>
          <h1>Planet {planet.name}</h1>
          {people.length ? (
            <div>
              <h3>Residents:</h3>
              {people && people.map(person => person && <Person {...person} key={person.url} />)}
            </div>
          ) :
            <span>No one lives here :(</span>
        }
        </div>
      );
    }
    return <Loading />;
  }
}

const mapStateToProps = (state, props) => {
  const planet = planetSelector(state, props);
  return {
    ...props,
    planet,
    people: peopleSelector(state, props),
    ready: planet && planet.ready,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchPlanet: (id) => dispatch(fetchPlanet(id)),
  fetchResidents: (residents) => residents.forEach(url => dispatch(fetchPerson(url)))
});

export default connect(mapStateToProps, mapDispatchToProps)(PeopleContainer);
