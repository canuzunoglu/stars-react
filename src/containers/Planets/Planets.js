import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { fetchPlanetsList, planetsPageSelector } from '../../ducks/planetsList';
import { parsePlanetId } from '../../ducks/planets';
import Planet from '../../components/Planet';
import Loading from '../../components/Loading';
import defaultStyles from './Planets.css';

export class PlanetsContainer extends Component {
  static propTypes = {
    fetchPlanetsList: PropTypes.func.isRequired,
    planets: PropTypes.arrayOf(PropTypes.object),
    prevPage: PropTypes.string,
    nextPage: PropTypes.string,
    ready: PropTypes.bool,
    styles: PropTypes.object,
  };

  static defaultProps = {
    styles: defaultStyles,
    ready: false,
    prevPage: null,
    nextPage: null,
    planets: [],
  }

  componentDidMount() {
    const { params: { page } } = this.props;
    this.props.fetchPlanetsList(page);
  }

  componentWillReceiveProps({ params: { page } }) {
    this.props.fetchPlanetsList(page);
  }

  render() {
    const { styles, ready, planets, prevPage, nextPage } = this.props;
    if (ready && planets) {
      return (
        <div>
          <h1>Planets</h1>
          {planets.map(planet =>
            <Planet {...planet} url={`/planet/${parsePlanetId(planet.url)}`} key={planet.url} />
          )}
          <div className={styles.navigaton}>
            {prevPage && <Link to={`/${prevPage}`} className={styles.prevButton}>Geri</Link>}
            {nextPage && <Link to={`/${nextPage}`} className={styles.nextButton}>Ileri</Link>}
          </div>
        </div>
      );
    }
    return <Loading />;
  }
}

const mapStateToProps = (state, props) => ({
  ...props,
  ...planetsPageSelector(state, props),
});

const mapDispatchToProps = (dispatch) => ({
  fetchPlanetsList: (page) => dispatch(fetchPlanetsList(page)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PlanetsContainer);
