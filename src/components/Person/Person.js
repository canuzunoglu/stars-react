import React, { PropTypes } from 'react';
import defaultStyles from './Person.css';

export default function Person({ styles, name, birth_year, eye_color, height, mass, gender }) {
  return (
    <div className={styles.component}>
      <div className={styles.title}>{name}</div>
      {gender !== 'unknown' && <div id="gender">Gender: {gender}</div>}
      {height !== 'unknown' && <div id="height">Height: {height}</div>}
      {mass !== 'unknown' && <div id="mass">Mass: {mass}</div>}
      {birth_year !== 'unknown' && <div id="birth_year">Birth Year: {birth_year}</div>}
      {eye_color !== 'unknown' && <div id="eye_color">Eye Color: {eye_color}</div>}
    </div>
  );
}

Person.defaultProps = {
  styles: defaultStyles,
};

Person.propTypes = {
  styles: PropTypes.object,
  name: PropTypes.string,
};
