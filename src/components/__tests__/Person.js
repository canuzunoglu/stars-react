import React from 'react';
import { shallow } from 'enzyme';
import Person from '../Person';

const data = {
  name: 'Test',
  gender: 'male',
  height: 110,
  mass: 90,
  birth_year: 1230,
};

describe('<Person />', () => {
  it('should render gender when it receives "gender" prop', () => {
    const wrapper = shallow(<Person {...data} />);
    expect(wrapper.find('#gender').text()).toContain(data.gender);
  });

  it('should not render gender when it receives "gender" unknown', () => {
    const wrapper = shallow(<Person {...data} gender="unknown" />);
    expect(wrapper.find('#gender').exists()).toBe(false);
  });

  it('should render height when it receives "height" prop', () => {
    const wrapper = shallow(<Person {...data} />);
    expect(wrapper.find('#height').text()).toContain(data.height);
  });

  it('should not render height when it receives "height" unknown', () => {
    const wrapper = shallow(<Person {...data} height="unknown" />);
    expect(wrapper.find('#height').exists()).toBe(false);
  });
});
