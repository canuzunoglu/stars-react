import React, { PropTypes } from 'react';
import { PulseLoader as Loader } from 'halogen';
import defaultStyles from './Loading.css';

export default function Loading({ styles }) {
  return (
    <div className={styles.loading}>
      <Loader color="#fcc419" size="16px" />
    </div>
  );
}

Loading.propTypes = {
  styles: PropTypes.object,
};

Loading.defaultProps = {
  styles: defaultStyles,
};
