import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import defaultStyles from './Planet.css';

export default function Planet({ styles, url, name, gravity,
  rotation_period, orbital_period, climate }) {
  return (
    <Link to={url} className={styles.component}>
      <div className={styles.title}>{name}</div>
      {climate !== 'unknown' && <div>Climate: {climate}</div>}
      {rotation_period !== 'unknown' && <div>Rotation Period: {rotation_period}</div>}
      {rotation_period !== 'unknown' && <div>Orbital Peeriod: {rotation_period}</div>}
    </Link>
  );
}

Planet.defaultProps = {
  styles: defaultStyles,
};

Planet.propTypes = {
  styles: PropTypes.object,
  url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
