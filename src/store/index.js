// Create final store using all reducers and applying middleware
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import promiseMiddleware from 'redux-promise';
// Redux utility functions
import { createStore, combineReducers, applyMiddleware } from 'redux';

// Import all reducers
import reducers from '../ducks';

const rootReducer = combineReducers(reducers);

const middlewares = [promiseMiddleware, thunk, logger()];

const store = createStore(
  rootReducer,
  applyMiddleware(...middlewares),
);

export default store;
