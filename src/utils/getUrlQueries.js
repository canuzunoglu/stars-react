import url from 'url';
import queryString from 'querystring';
import { compose } from 'redux';

export default compose(queryString.parse, ({ query }) => query, url.parse);
